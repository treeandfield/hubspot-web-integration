<?php
$cookie = null;
if(isset($_COOKIE['hubspotutk'])){
	$cookie   = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.	
}

$hubspotutk      = $cookie;
$ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
$hs_context      = array(
    'hutk' => $hubspotutk,
    'ipAddress' => $ip_addr,
    'pageUrl' => $page,
    'pageName' => $pageName
);

$hs_context_json = json_encode($hs_context);
?>